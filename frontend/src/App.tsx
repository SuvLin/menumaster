import Container from "react-bootstrap/esm/Container";
import { Col, Row } from "react-bootstrap";
import { Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { AddUserForm } from "./components/AddUserForm";
import { Search } from "./components/Search";
import { Navigation } from "./components/Navigation";
import { SelectUser } from "./components/SelectUser";
import { UserProvider } from "./context/UserProvider";
import { UserForms } from "./components/UserForms";
import { Recipes } from "./components/Recipes";
import { AddRecipeForm } from "./components/AddRecipeForm";
import { GenerateMenus } from "./components/GenerateMenus";
import { EditRecipeForm } from "./components/EditRecipeForm";
import { SearchProvider } from "./context/SearchProvider";
import { ProfileImage } from "./components/ProfileImage";
import { Favorites } from "./components/Favorites";

function App() {
    return (
        <>
            <Container fluid>
                <Row className="justify-content-md-center mt-5 pt-5">
                    <UserProvider>
                        <SearchProvider>
                            <Col xs={12} md={5} style={{ maxWidth: "400px" }}>
                                <Row className="m-3">
                                    <Col>
                                        <ProfileImage />
                                    </Col>
                                    <Col>
                                        <SelectUser />
                                    </Col>
                                </Row>
                                <Row className="m-2">
                                    <Search />
                                </Row>
                                <Row className="m-2">
                                    <Navigation />
                                </Row>
                            </Col>
                            <Col xs={12} md={7}>
                                <Routes>
                                    <Route
                                        path="/"
                                        element={<div>Welcome!</div>}
                                    />
                                    <Route
                                        path="/add-user"
                                        element={<AddUserForm />}
                                    />
                                    <Route
                                        path="/edit-user/:userId"
                                        element={<UserForms />}
                                    />
                                    <Route
                                        path="/recipes"
                                        element={<Recipes />}
                                    />
                                    <Route
                                        path="/add-recipe"
                                        element={<AddRecipeForm />}
                                    />
                                    <Route
                                        path="/edit-recipe/:recipeId"
                                        element={<EditRecipeForm />}
                                    />
                                    <Route
                                        path="/generate-menus"
                                        element={<GenerateMenus />}
                                    />
                                    <Route
                                        path="/users/:userid/favorites"
                                        element={<Favorites />}
                                    />
                                </Routes>
                            </Col>
                        </SearchProvider>
                    </UserProvider>
                </Row>
            </Container>
        </>
    );
}

export default App;
