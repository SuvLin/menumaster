import axios from "axios";

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

const apiUrl = import.meta.env.VITE_API_URL;

function isError(error: unknown): error is Error {
    return error instanceof Error;
}

export const getRecipe = async (recipe_id: number): Promise<Recipe> => {
    try {
        const response = await axios.get(`${apiUrl}/recipes/${recipe_id}`);
        return response.data;
    } catch (error) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred");
            throw new Error("An unexpected error occurred");
        }
    }
};

export const getAllRecipes = async (): Promise<Recipe[]> => {
    try {
        const response = await axios.get(`${apiUrl}/recipes/`);
        return response.data;
    } catch (error: unknown) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred");
            throw new Error("An unexpected error occurred");
        }
    }
};

export const getAllRecipesWithKeyword = async (keyword: string) => {
    try {
        const response = await axios.get(`${apiUrl}/recipes/search`, {
            params: {
                keyword,
            },
        });
        return response.data;
    } catch (error: unknown) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred when searching");
            throw new Error("An unexpected error occurred when searching");
        }
    }
};

export const addRecipe = async (
    title: string,
    link?: string,
    notes?: string[]
): Promise<void> => {
    try {
        const response = await axios.post(`${apiUrl}/recipes/`, {
            recipe_title: title,
            recipe_link: link,
            recipe_notes: notes,
        });
        const newRecipe = response.data;
        return newRecipe;
    } catch (error) {
        console.error(`Error: ${error}`);
        throw error;
    }
};

export const editRecipe = async (
    id: number,
    newTitle: string,
    newLink?: string,
    newNotes?: string[]
): Promise<void> => {
    try {
        const response = await axios.put(`${apiUrl}/recipes/${id}`, {
            recipe_title: newTitle,
            recipe_link: newLink,
            recipe_notes: newNotes,
        });
        const updatedRecipe = response.data;
        return updatedRecipe;
    } catch (error) {
        console.error(`Error: ${error}`);
        throw error;
    }
};

export const deleteRecipeById = async (id: number): Promise<void> => {
    try {
        const response = await axios.delete(`${apiUrl}/recipes/${id}`);
        const deletedRecipe = response.data;
        return deletedRecipe;
    } catch (error) {
        console.error(`Error: ${error}`);
        throw error;
    }
};
