import axios from "axios";
const apiUrl = import.meta.env.VITE_API_URL;

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

export const AddToFavorites = async (
    userId: number,
    recipeId: number
): Promise<void> => {
    try {
        const response = await axios.post(
            `${apiUrl}/users/${userId}/favorites`,
            {
                recipeId,
            }
        );
        console.log("Added to favorites:", response.data);
    } catch (error) {
        console.error("Failed to add to favorites:", error);
    }
};

export const fetchFavorites = async (userId: number): Promise<Recipe[]> => {
    try {
        const response = await axios.get(
            `${apiUrl}/users/${userId}/favorites/`
        );
        console.log("Favorites:", response.data);
        return response.data;
    } catch (error) {
        console.error("Failed to fetch favorites:", error);
        return [];
    }
};

export const checkIfFavorite = async (userId: number, recipeId: number) => {
    try {
        const response = await axios.get(
            `${apiUrl}/users/${userId}/favorites/check/${recipeId}`
        );
        return response.data.isFavorite;
    } catch (error) {
        console.error("Failed to check favorite status:", error);
        return false;
    }
};

export const removeFromFavorites = async (userId: number, recipeId: number) => {
    try {
        const response = await axios.delete(
            `${apiUrl}/users/${userId}/favorites/check/${recipeId}`
        );
        console.log(`Recipe deleted from favorites`, response.data);
        return response.data;
    } catch (error) {
        console.error("Failed to remove from favorites:", error);
    }
};
