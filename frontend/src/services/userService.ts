import axios from "axios";

interface User {
    username: string;
    user_id: number;
}

const apiUrl = import.meta.env.VITE_API_URL;

function isError(error: unknown): error is Error {
    return error instanceof Error;
}

export const getAllUsers = async (): Promise<User[]> => {
    try {
        const response = await axios.get(`${apiUrl}/users`);
        return response.data;
    } catch (error: unknown) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred");
            throw new Error("An unexpected error occurred");
        }
    }
};

export const addUser = async (username: string): Promise<void> => {
    try {
        const response = await axios.post(`${apiUrl}/users`, {
            username: username,
        });

        const newUser = response.data;
        return newUser;
    } catch (error: unknown) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred");
            throw new Error("An unexpected error occurred");
        }
    }
};

export const getUserById = async (user_id: number): Promise<User> => {
    try {
        const response = await axios.get(`${apiUrl}/users/${user_id}`);
        return response.data;
    } catch (error: unknown) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred");
            throw new Error("An unexpected error occurred");
        }
    }
};

export const editUser = async (
    newUserName: string,
    user_id: number
): Promise<void> => {
    try {
        const response = await axios.put(`${apiUrl}/users/${user_id}`, {
            username: newUserName,
        });
        const updatedUser = response.data;
        return updatedUser;
    } catch (error: unknown) {
        if (isError(error)) {
            console.error(error.message);
            throw new Error(error.message);
        } else {
            console.error("An unexpected error occurred");
            throw new Error("An unexpected error occurred");
        }
    }
};
