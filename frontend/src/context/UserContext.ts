import { createContext, useContext } from "react";

interface UserContextProps {
    selectedUserId: string | null;
    setSelectedUserId: (id: string | null) => void;
}

export const UserContext = createContext<UserContextProps | undefined>(
    undefined
);

export const useUser = () => {
    const context = useContext(UserContext);
    if (context === undefined) {
        throw new Error("useUser must be used within a UserProvider");
    }
    return context;
};
