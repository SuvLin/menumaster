import { useState, useEffect, ReactNode } from "react";
import { UserContext } from "./UserContext";

interface UserProviderProps {
    children: ReactNode;
}

export const UserProvider: React.FC<UserProviderProps> = ({ children }) => {
    const [selectedUserId, setSelectedUserId] = useState<string | null>(null);

    useEffect(() => {
        const storedId = localStorage.getItem("selectedUserId");
        if (storedId) {
            setSelectedUserId(storedId);
        }
    }, []);

    useEffect(() => {
        if (selectedUserId !== null) {
            localStorage.setItem("selectedUserId", selectedUserId);
        }
    }, [selectedUserId]);

    return (
        <UserContext.Provider value={{ selectedUserId, setSelectedUserId }}>
            {children}
        </UserContext.Provider>
    );
};
