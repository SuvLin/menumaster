import { createContext } from "react";

export interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

export interface SearchContextProps {
    searchResult: Recipe[];
    isLoading: boolean;
    searchPerformed: boolean;
    handleSearch: (searchKeyword: string) => Promise<void>;
}

const defaultState: SearchContextProps = {
    searchResult: [],
    isLoading: false,
    searchPerformed: false,
    handleSearch: async () => {},
};

export const SearchContext = createContext<SearchContextProps>(defaultState);
