import { ReactNode, useState } from "react";
import { getAllRecipesWithKeyword } from "../services/recipeService";
import { SearchContext, SearchContextProps, Recipe } from "./SearchContext";

interface SearchProviderProps {
    children: ReactNode;
}

export const SearchProvider = ({ children }: SearchProviderProps) => {
    const [searchResult, setSearchResult] = useState<Recipe[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [searchPerformed, setSearchPerformed] = useState<boolean>(false);

    const handleSearch = async (searchKeyword: string) => {
        setIsLoading(true);
        const results = await getAllRecipesWithKeyword(searchKeyword);
        setSearchPerformed(true);
        setSearchResult(results);
        setIsLoading(false);
    };

    const contextValue: SearchContextProps = {
        searchResult,
        isLoading,
        searchPerformed,
        handleSearch,
    };

    return (
        <SearchContext.Provider value={contextValue}>
            {children}
        </SearchContext.Provider>
    );
};
