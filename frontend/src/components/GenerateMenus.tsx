import ListGroup from "react-bootstrap/ListGroup";
import { Button, Card, Col, Form, Row } from "react-bootstrap";
import { useGetRecipes } from "../hooks/useGetRecipes";
import { useState } from "react";
import { LoadingSpinner } from "./LoadingSpinner";

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

export const GenerateMenus = () => {
    const [generatedMenu, setGeneratedMenu] = useState<Recipe[][]>([]);
    const [isMenuLoading, setIsMenuLoading] = useState<boolean>(false);
    const [isButtonClicked, setIsButtonClicked] = useState<boolean>(false);
    const [numberOfMenus, setNumberOfMenus] = useState<number>(1);

    const { recipeList, isLoading } = useGetRecipes();

    const shuffleRecipes = (menuArray: Recipe[]) => {
        for (let i = menuArray.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [menuArray[i], menuArray[j]] = [menuArray[j], menuArray[i]];
        }
        return menuArray;
    };

    const generateMenu = () => {
        setIsButtonClicked(true);
        setIsMenuLoading(true);
        setGeneratedMenu([]);

        setTimeout(() => {
            const menus: Recipe[][] = [];
            const shuffledArray = shuffleRecipes([...recipeList]);

            //logic for not repeating same recipes in different menus
            for (let i = 0; i < numberOfMenus; i++) {
                const startIndex = i * 3;
                const endIndex = startIndex + 3;
                const newMenu = shuffledArray.slice(startIndex, endIndex);
                menus.push(newMenu);
            }
            setGeneratedMenu(menus);
            setIsMenuLoading(false);
        }, 1000);
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNumberOfMenus(Number(e.target.value));
    };

    return (
        <>
            {isLoading ? (
                <div className="d-flex justify-content-center align-items-center">
                    <LoadingSpinner />;
                </div>
            ) : (
                <>
                    <Row>
                        <Col>
                            <Form>
                                <Button
                                    variant="primary"
                                    onClick={generateMenu}
                                    className="m-2"
                                >
                                    Generate menu
                                </Button>
                                <p>Number of menus to generate:</p>
                                <div className="mb-3">
                                    {[1, 2, 3, 4].map((number) => (
                                        <Form.Check
                                            key={number}
                                            type="radio"
                                            inline
                                            label={`${number}`}
                                            name="weeks"
                                            value={number}
                                            checked={numberOfMenus === number}
                                            onChange={handleChange}
                                        />
                                    ))}
                                </div>
                            </Form>{" "}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {isButtonClicked &&
                                (isMenuLoading ? (
                                    <LoadingSpinner />
                                ) : (
                                    generatedMenu.map((menu, index) => (
                                        <Col
                                            key={index}
                                            className="m-3"
                                            style={{ maxWidth: "350px" }}
                                        >
                                            <Card key={index}>
                                                <Card.Body>
                                                    <Card.Title>
                                                        Week {index + 1}
                                                    </Card.Title>
                                                    <ListGroup variant="flush">
                                                        {menu.map(
                                                            (
                                                                recipe: Recipe
                                                            ) => (
                                                                <ListGroup.Item
                                                                    key={
                                                                        recipe.recipe_id
                                                                    }
                                                                >
                                                                    {
                                                                        recipe.recipe_title
                                                                    }
                                                                </ListGroup.Item>
                                                            )
                                                        )}
                                                    </ListGroup>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    ))
                                ))}
                        </Col>
                    </Row>
                </>
            )}
        </>
    );
};
