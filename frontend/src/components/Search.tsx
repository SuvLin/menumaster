import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { InputGroup } from "react-bootstrap";
import { useState } from "react";
import { useSearch } from "../hooks/useSearch";

export const Search = () => {
    const [localKeyword, setLocalKeyword] = useState<string>("");
    const { handleSearch } = useSearch();

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLocalKeyword(e.target.value);
    };

    const handleSubmit = async (
        e:
            | React.MouseEvent<HTMLButtonElement>
            | React.FormEvent<HTMLFormElement>
    ) => {
        e.preventDefault();
        handleSearch(localKeyword);
    };

    return (
        <>
            <Form>
                <InputGroup>
                    <Form.Control
                        type="text"
                        placeholder="Search..."
                        id="searchInput"
                        value={localKeyword}
                        onChange={handleChange}
                    />
                </InputGroup>
                <Button
                    variant="outline-secondary"
                    type="submit"
                    className="mt-2"
                    onClick={handleSubmit}
                >
                    Search
                </Button>
            </Form>
        </>
    );
};
