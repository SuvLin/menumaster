import { useUser } from "../context/UserContext";
import { EditUserForm } from "./EditUserForm";
import { AddUserForm } from "./AddUserForm";

export const UserForms = () => {
    const { selectedUserId } = useUser();
    return <>{selectedUserId ? <EditUserForm /> : <AddUserForm />}</>;
};
