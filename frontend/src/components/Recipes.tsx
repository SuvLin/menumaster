import { Col, ListGroup } from "react-bootstrap";
import { useGetRecipes } from "../hooks/useGetRecipes";
import { LoadingSpinner } from "./LoadingSpinner";
import { Fragment, useState } from "react";
import { RecipeCard } from "./RecipeCard";
import { useSearch } from "../hooks/useSearch";
import { NoSearchResults } from "./NoSearchResults";
import { FavoritesIcon } from "./FavoritesIcon";

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

export const Recipes = () => {
    const {
        recipeList,
        isLoading: isRecipesLoading,
        refetch,
    } = useGetRecipes();
    const { searchResult, isLoading: isSearchLoading } = useSearch();
    const [selectedRecipe, setSelectedRecipe] = useState<number | null>(null);

    const handleRecipeDeleted = async (deletedRecipeId: number) => {
        refetch();
        console.log(deletedRecipeId, " is deleted");
        setSelectedRecipe(null);
    };

    const displayRecipes = searchResult.length > 0 ? searchResult : recipeList;
    const isLoading = isRecipesLoading || isSearchLoading;

    return (
        <>
            {isLoading ? (
                <div className="d-flex justify-content-center align-items-center">
                    <LoadingSpinner />
                </div>
            ) : (
                <Col className="m-2" style={{ maxWidth: "400px" }}>
                    <h1>List of recipes</h1>
                    <ListGroup>
                        {displayRecipes.map((recipe: Recipe) => (
                            <Fragment key={recipe.recipe_id}>
                                <ListGroup.Item
                                    key={recipe.recipe_id}
                                    action
                                    className="d-flex justify-content-between"
                                    onClick={() =>
                                        setSelectedRecipe(
                                            selectedRecipe === recipe.recipe_id
                                                ? null
                                                : recipe.recipe_id
                                        )
                                    }
                                >
                                    {recipe.recipe_title}
                                    <FavoritesIcon
                                        recipeId={recipe.recipe_id}
                                    />
                                </ListGroup.Item>
                                {selectedRecipe === recipe.recipe_id && (
                                    <RecipeCard
                                        recipeId={selectedRecipe}
                                        show={selectedRecipe !== null}
                                        onDelete={handleRecipeDeleted}
                                    />
                                )}
                            </Fragment>
                        ))}
                    </ListGroup>
                </Col>
            )}
            <NoSearchResults />
        </>
    );
};
