import { FaHeart, FaRegHeart } from "react-icons/fa";
import { useUser } from "../context/UserContext";
import { useEffect, useState } from "react";
import {
    AddToFavorites,
    checkIfFavorite,
    removeFromFavorites,
} from "../services/favoriteService";
import "../style/button.css";

interface FavoritesIconProps {
    recipeId: number;
}

export const FavoritesIcon = ({ recipeId }: FavoritesIconProps) => {
    const { selectedUserId } = useUser();
    const [isFavorite, setIsFavorite] = useState<boolean>(false);

    const parsedUserId = selectedUserId ? parseInt(selectedUserId) : null;

    useEffect(() => {
        const checkFavoriteStatus = async () => {
            if (parsedUserId !== null) {
                const status = await checkIfFavorite(parsedUserId, recipeId);
                setIsFavorite(status);
            }
        };
        checkFavoriteStatus();
    }, [recipeId, parsedUserId]);

    const handleClick = async (e: React.MouseEvent) => {
        e.preventDefault();
        e.stopPropagation();

        if (parsedUserId !== null) {
            isFavorite
                ? await removeFromFavorites(parsedUserId, recipeId)
                : await AddToFavorites(parsedUserId, recipeId);
            const status = await checkIfFavorite(parsedUserId, recipeId);
            setIsFavorite(status);
        }
    };

    return (
        <div onClick={handleClick} className="fav-button">
            {isFavorite ? (
                <FaHeart className="fav-icon favorite" />
            ) : (
                <FaRegHeart className="fav-icon" />
            )}
        </div>
    );
};
