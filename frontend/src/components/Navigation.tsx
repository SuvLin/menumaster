import { Dropdown, Button, ButtonGroup } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useUser } from "../context/UserContext";

export const Navigation = () => {
    const { selectedUserId } = useUser();
    const navigate = useNavigate();

    return (
        <div className="d-flex justify-content-between">
            <Button
                variant="primary"
                type="submit"
                className="mb-2 mt-2 me-2"
                onClick={() => navigate(`/generate-menus`)}
            >
                Menus
            </Button>{" "}
            <Button
                variant="primary"
                type="submit"
                className="m-2"
                onClick={() => navigate(`/users/${selectedUserId}/favorites`)}
            >
                Favorites
            </Button>
            <Dropdown as={ButtonGroup} className="m-2">
                <Button
                    variant="primary"
                    type="submit"
                    onClick={() => navigate(`/recipes`)}
                >
                    Recipes
                </Button>
                <Dropdown.Toggle split variant="primary" />
                <Dropdown.Menu>
                    <Dropdown.Item onClick={() => navigate(`/add-recipe`)}>
                        Add recipe
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
    );
};
