import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import { getAllUsers } from "../services/userService";
import { useEffect, useState } from "react";
import { useUser } from "../context/UserContext";
import { useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";

interface User {
    user_id: number;
    username: string;
}
export const SelectUser = () => {
    const [userList, setUserList] = useState<User[]>([]);
    const { selectedUserId, setSelectedUserId } = useUser();
    const [selectedUserName, setSelectedUserName] =
        useState<string>("Select User");
    const navigate = useNavigate();

    useEffect(() => {
        const getUsers = async () => {
            try {
                const users = await getAllUsers();

                setUserList(users);

                if (selectedUserId) {
                    const selectedUser = users.find(
                        (user) => user.user_id.toString() === selectedUserId
                    );
                    if (selectedUser) {
                        setSelectedUserName(selectedUser.username);
                    }
                }
            } catch (error) {
                console.error(`Couldn't get users: ${error}`);
            }
        };
        getUsers();
    }, [selectedUserId]);

    const handleSelectUser = (user: User) => {
        setSelectedUserId(user.user_id.toString());
        setSelectedUserName(user.username);
    };

    return (
        <>
            <DropdownButton
                id="dropdown-basic-button"
                title={selectedUserName}
                className="mt-2 mb-4"
            >
                {userList.map((user) => (
                    <Dropdown.Item
                        key={user.user_id}
                        onClick={() => handleSelectUser(user)}
                    >
                        {user.username}
                    </Dropdown.Item>
                ))}
            </DropdownButton>
            <Button
                variant="success"
                size="sm"
                onClick={() => navigate(`/add-user`)}
                className="mb-2"
            >
                Create new user
            </Button>
            <Button
                variant="link"
                size="sm"
                onClick={() => {
                    if (selectedUserId) {
                        navigate(`/edit-user/${selectedUserId}`);
                    } else {
                        alert("Please select a user to edit.");
                    }
                }}
                className="mb-2"
            >
                Edit user
            </Button>
        </>
    );
};
