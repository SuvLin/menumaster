import myImage from "../img/littleChef2.png";
import Image from "react-bootstrap/Image";

export const ProfileImage = () => {
    return <Image src={myImage} roundedCircle fluid />;
};
