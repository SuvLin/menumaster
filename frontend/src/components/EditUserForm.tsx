import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { editUser, getUserById } from "../services/userService";
import { useEffect, useState } from "react";
import { LoadingSpinner } from "./LoadingSpinner";
import { useUser } from "../context/UserContext";
import { useNavigate } from "react-router-dom";

interface User {
    user_id: number;
    username: string;
}

export const EditUserForm = () => {
    const { selectedUserId } = useUser();
    const [newUserName, setNewUserName] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        if (selectedUserId) {
            const id = parseInt(selectedUserId);

            const getUser = async (): Promise<void> => {
                setIsLoading(true);
                try {
                    const user: User = await getUserById(id);
                    setNewUserName(user.username);
                    setIsLoading(false);
                } catch (error) {
                    console.error("Error getting user information:", error);
                    setIsLoading(false);
                }
            };
            getUser();
        }
    }, [selectedUserId]);

    const handleSubmit = async (e: React.FormEvent<Element>): Promise<void> => {
        e.preventDefault();
        try {
            if (selectedUserId) {
                const id = parseInt(selectedUserId);
                const newUser = await editUser(newUserName, id);
                console.log("User updated", newUser);
                //NOTE(Suvi): Here some indication for success
                //navigate("/");
            }
        } catch (error) {
            console.error("Failed to update user:", error);
        }
    };

    return (
        <>
            {isLoading ? (
                <div className="d-flex justify-content-center align-items-center">
                    <LoadingSpinner />;
                </div>
            ) : (
                <Form
                    onSubmit={handleSubmit}
                    className="m-2"
                    style={{ maxWidth: "400px" }}
                >
                    <Form.Group className="mb-3">
                        <h1>Edit user</h1>
                        <Form.Label htmlFor="nameInput">Name</Form.Label>
                        <Form.Control
                            type="name"
                            id="nameInput"
                            value={newUserName}
                            onChange={(e) => setNewUserName(e.target.value)}
                        />
                    </Form.Group>
                    <div className="d-flex justify-content-end">
                        <Button variant="primary" type="submit" className="m-2">
                            Save
                        </Button>
                        <Button
                            variant="danger"
                            className="m-2"
                            onClick={() => navigate("/")}
                        >
                            Cancel
                        </Button>
                    </div>
                </Form>
            )}
        </>
    );
};
