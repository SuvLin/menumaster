import { useEffect, useState } from "react";
import { Form, Button, CloseButton } from "react-bootstrap";
import { editRecipe, getRecipe } from "../services/recipeService";
import { useNavigate, useParams } from "react-router-dom";
import { LoadingSpinner } from "./LoadingSpinner";

export const EditRecipeForm = () => {
    const [recipeTitle, setRecipeTitle] = useState<string>("");
    const [recipeLink, setRecipeLink] = useState<string>("");
    const [recipeNotes, setRecipeNotes] = useState<string[]>([""]);
    const { recipeId } = useParams<{
        recipeId: string;
    }>();
    const [isRecipeLoading, setIsRecipeLoading] = useState<boolean>(false);

    const navigate = useNavigate();

    useEffect(() => {
        const getRecipeData = async () => {
            if (recipeId) {
                setIsRecipeLoading(true);
                try {
                    const recipeData = await getRecipe(parseInt(recipeId));
                    setRecipeTitle(recipeData.recipe_title);
                    setRecipeLink(recipeData.recipe_link || "");
                    setRecipeNotes(recipeData.recipe_notes || [""]);
                } catch (error) {
                    console.error("Error fetching recipe details:", error);
                }
                setIsRecipeLoading(false);
            }
        };
        getRecipeData();
    }, [recipeId]);

    const handleNoteChange = (text: string, index: number) => {
        const newNotes = [...recipeNotes];
        newNotes[index] = text;
        setRecipeNotes(newNotes);
    };

    const addNoteInput = () => {
        setRecipeNotes([...recipeNotes, ""]);
    };

    const removeNoteInput = (index: number) => {
        const newNotes = [...recipeNotes];
        newNotes.splice(index, 1);
        setRecipeNotes(newNotes);
    };

    const handleSubmit = async (e: React.FormEvent<Element>) => {
        e.preventDefault();
        setIsRecipeLoading(true);

        try {
            if (recipeId) {
                const selectedRecipeId = parseInt(recipeId);
                const editedRecipe = await editRecipe(
                    selectedRecipeId,
                    recipeTitle,
                    recipeLink,
                    recipeNotes
                );
                console.log("Recipe updated:", editedRecipe);
                navigate("/recipes");
            }
        } catch (error) {
            console.error("Failed to update recipe:", error);
        } finally {
            setIsRecipeLoading(false);
        }
    };

    return (
        <>
            {isRecipeLoading ? (
                <div className="d-flex justify-content-center align-items-center">
                    <LoadingSpinner />;
                </div>
            ) : (
                <Form
                    onSubmit={handleSubmit}
                    className="m-2"
                    style={{ maxWidth: "400px" }}
                >
                    <Form.Group className="mb-3">
                        <h1>Edit recipe</h1>
                        <Form.Label>Recipe title</Form.Label>
                        <Form.Control
                            required
                            type=""
                            id="recipeName"
                            placeholder="Type title of recipe"
                            value={recipeTitle}
                            onChange={(e) => setRecipeTitle(e.target.value)}
                        ></Form.Control>
                        <Form.Label className="mt-3">Recipe link</Form.Label>
                        <Form.Control
                            type=""
                            id="recipeLink"
                            placeholder="Type link to recipe"
                            value={recipeLink}
                            onChange={(e) => setRecipeLink(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Label>Recipe notes</Form.Label>
                    {recipeNotes.map((note, index) => (
                        <Form.Group
                            key={index}
                            className="mb-3 d-flex align-items-center"
                        >
                            <Form.Control
                                type="text"
                                placeholder="Enter note"
                                value={note}
                                onChange={(e) =>
                                    handleNoteChange(e.target.value, index)
                                }
                                className="me-2"
                            />

                            <CloseButton
                                onClick={() => removeNoteInput(index)}
                            />
                        </Form.Group>
                    ))}
                    <Button
                        variant="secondary"
                        onClick={addNoteInput}
                        className="mb-3"
                    >
                        Add Note
                    </Button>
                    <div className="d-flex justify-content-end">
                        <Button variant="success" type="submit" className="m-2">
                            Save
                        </Button>
                        <Button
                            variant="danger"
                            className="m-2"
                            onClick={() => navigate("/recipes")}
                        >
                            Cancel
                        </Button>
                    </div>
                </Form>
            )}
        </>
    );
};
