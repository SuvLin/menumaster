import { Button, Card, ListGroup } from "react-bootstrap";
import { deleteRecipeById, getRecipe } from "../services/recipeService";
import { useEffect, useState } from "react";
import { ConfirmModal } from "./ConfirmModal";
import { useNavigate } from "react-router-dom";
import { FavoritesIcon } from "./FavoritesIcon";

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

interface RecipeCardProps {
    recipeId: number;
    show: boolean;
    onDelete: (id: number) => void;
}

export const RecipeCard = ({ recipeId, onDelete }: RecipeCardProps) => {
    const [recipe, setRecipe] = useState<Recipe | null>(null);
    const [showConfirm, setShowConfirm] = useState(false);

    const navigate = useNavigate();

    useEffect(() => {
        const fetchRecipe = async () => {
            const fetchedRecipe = await getRecipe(recipeId);
            setRecipe(fetchedRecipe);
        };
        if (recipeId) {
            fetchRecipe();
        }
    }, [recipeId]);

    const hasNotes =
        (recipe?.recipe_notes?.filter((note) => note.trim() !== "") ?? [])
            .length > 0;

    const deleteRecipe = async () => {
        try {
            await deleteRecipeById(recipeId);
            console.log("Recipe deleted");
            onDelete(recipeId);
        } catch (error) {
            console.error(`Error: ${error}`);
            throw error;
        }
    };

    if (!recipe) return null;

    return (
        <Card className="mt-2">
            <Card.Body>
                <div className="d-flex justify-content-between">
                    {recipe?.recipe_link && (
                        <Button href={recipe?.recipe_link} className="mb-2">
                            To the recipe
                        </Button>
                    )}
                    <FavoritesIcon recipeId={recipe.recipe_id} />
                </div>
                {hasNotes && (
                    <ListGroup className="mt-2">
                        {recipe?.recipe_notes?.map((note, index) => (
                            <ListGroup.Item key={index}>{note}</ListGroup.Item>
                        ))}
                    </ListGroup>
                )}
            </Card.Body>
            <Card.Footer className="d-flex justify-content-end">
                <Button
                    variant="success"
                    className="m-2"
                    onClick={() => navigate(`/edit-recipe/${recipeId}`)}
                >
                    Edit
                </Button>
                <Button
                    variant="danger"
                    className="m-2"
                    onClick={() => setShowConfirm(true)}
                >
                    Delete
                </Button>
                <ConfirmModal
                    show={showConfirm}
                    title="Confirm Delete"
                    message="Are you sure you want to delete this item?"
                    onConfirm={deleteRecipe}
                    onCancel={() => setShowConfirm(false)}
                />
            </Card.Footer>
        </Card>
    );
};
