import { Form, Button } from "react-bootstrap";
import { addUser } from "../services/userService";
import { useState } from "react";

export const AddUserForm = () => {
    const [userName, setUserName] = useState("");

    const handleSubmit = async (e: React.FormEvent<Element>): Promise<void> => {
        e.preventDefault();
        try {
            const newUser = await addUser(userName);
            console.log("User added:", newUser);
            setUserName("");
        } catch (error) {
            console.error("Error adding user:", error);
        }
    };

    return (
        <Form
            onSubmit={handleSubmit}
            className="m-2"
            style={{ maxWidth: "400px" }}
        >
            <Form.Group className="mb-3">
                <h1>Create new user</h1>
                <Form.Label>Name</Form.Label>
                <Form.Control
                    type="name"
                    placeholder="Type your name"
                    value={userName}
                    onChange={(e) => setUserName(e.target.value)}
                />
            </Form.Group>
            <div className="d-flex justify-content-end">
                <Button variant="primary" type="submit" className="m-2">
                    Submit
                </Button>
                <Button
                    variant="danger"
                    className="m-2"
                    onClick={() => setUserName("")}
                >
                    Cancel
                </Button>
            </div>
        </Form>
    );
};
