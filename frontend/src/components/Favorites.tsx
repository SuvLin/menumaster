import { Col, ListGroup } from "react-bootstrap";
import { LoadingSpinner } from "./LoadingSpinner";
import { Fragment, useEffect, useState } from "react";
import { RecipeCard } from "./RecipeCard";
import { NoSearchResults } from "./NoSearchResults";
import { fetchFavorites } from "../services/favoriteService";
import { useUser } from "../context/UserContext";

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

export const Favorites = () => {
    const { selectedUserId } = useUser();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [selectedRecipe, setSelectedRecipe] = useState<number | null>(null);
    const [favorites, setFavorites] = useState<Recipe[]>([]);

    useEffect(() => {
        const loadFavorites = async () => {
            if (selectedUserId) {
                setIsLoading(true);
                const parsedId = parseInt(selectedUserId);
                try {
                    const fetchedFavorites = await fetchFavorites(parsedId);
                    setFavorites(fetchedFavorites);
                } catch (error) {
                    console.error("Failed to fetch favorites:", error);
                }
                setIsLoading(false);
            }
        };
        loadFavorites();
    }, [selectedUserId]);

    const handleRecipeDeleted = async (deletedRecipeId: number) => {
        // refetch();
        console.log(deletedRecipeId, " is deleted");
        setSelectedRecipe(null);
    };

    return (
        <>
            {isLoading ? (
                <div className="d-flex justify-content-center align-items-center">
                    <LoadingSpinner />
                </div>
            ) : (
                <Col className="m-2" style={{ maxWidth: "400px" }}>
                    <h1>Favorites</h1>
                    <ListGroup>
                        {favorites.map((recipe: Recipe) => (
                            <Fragment key={recipe.recipe_id}>
                                <ListGroup.Item
                                    key={recipe.recipe_id}
                                    action
                                    onClick={() =>
                                        setSelectedRecipe(
                                            selectedRecipe === recipe.recipe_id
                                                ? null
                                                : recipe.recipe_id
                                        )
                                    }
                                >
                                    {recipe.recipe_title}
                                </ListGroup.Item>
                                {selectedRecipe === recipe.recipe_id && (
                                    <RecipeCard
                                        recipeId={selectedRecipe}
                                        show={selectedRecipe !== null}
                                        onDelete={handleRecipeDeleted}
                                    />
                                )}
                            </Fragment>
                        ))}
                    </ListGroup>
                </Col>
            )}
            <NoSearchResults />
        </>
    );
};
