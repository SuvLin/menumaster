import { useContext, useEffect, useState } from "react";
import { SearchContext } from "../context/SearchContext";
import { Modal } from "react-bootstrap";

export const NoSearchResults = () => {
    const { searchResult, isLoading, searchPerformed } =
        useContext(SearchContext);
    const [showNoResultsModal, setShowNoResultsModal] = useState(false);

    useEffect(() => {
        if (searchPerformed && searchResult.length === 0 && !isLoading) {
            setShowNoResultsModal(true);
        } else {
            setShowNoResultsModal(false);
        }
    }, [searchResult, isLoading, searchPerformed]);

    return (
        <>
            <Modal
                show={showNoResultsModal}
                onHide={() => setShowNoResultsModal(false)}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Search Result</Modal.Title>
                </Modal.Header>
                <Modal.Body>No results found for your search.</Modal.Body>
            </Modal>
        </>
    );
};
