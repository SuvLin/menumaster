import { useCallback, useEffect, useState } from "react";
import { getAllRecipes } from "../services/recipeService";

interface Recipe {
    recipe_id: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

export const useGetRecipes = () => {
    const [recipeList, setRecipeList] = useState<Recipe[]>([]);
    const [isLoading, setIsLoading] = useState(false);

    const getRecipes = useCallback(async () => {
        setIsLoading(true);
        try {
            const recipes = await getAllRecipes();
            setRecipeList(recipes);
        } catch (error) {
            console.error(`Couldn't get recipes: ${error}`);
        } finally {
            setIsLoading(false);
        }
    }, []);

    useEffect(() => {
        getRecipes();
    }, [getRecipes]);

    return { recipeList, isLoading, setIsLoading, refetch: getRecipes };
};
