About
====================

MenuMaster is a full-stack web application designed to simplify meal planning and recipe management for individuals who struggle with meal planning and decision fatigue around daily meals.

Developed as a study project, it enables users to save their favorite recipes to a database and generate weekly or monthly meal plans. The platform features a user-friendly interface for easy navigation and recipe submission, and integrates functionalities for generating "What to Eat This Week?" menus, making meal preparation hassle-free.

MenuMaster is solo project started during full-stack developer studies in Trainee Academy held by Buutti.

### Next possible steps:

- Design UI and implement
- Implement auhtentication
- Create unit tests
- launch on server