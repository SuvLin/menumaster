import express from "express";
import dotenv from "dotenv";
import "dotenv/config";
import {
    createUserTable,
    createRecipesTable,
    createFavoritesTable,
} from "./db/db";
import {
    errorHandlerMiddleware,
    loggingMiddleware,
    notFoundMiddleware,
} from "./middleware/errorHandlers";
import userController from "./controllers/userController";
import recipesController from "./controllers/recipesController";
import favoritesController from "./controllers/favoritesController";
import cors from "cors";

dotenv.config();

const server = express();
const { PORT } = process.env;

//body parsing
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.use(cors());

createUserTable();
createRecipesTable();
createFavoritesTable();

server.use("/", userController);
server.use("/", recipesController);
server.use("/", favoritesController);

server.use(loggingMiddleware);
server.use(errorHandlerMiddleware);
server.use(notFoundMiddleware);

server.listen(PORT, () => console.log("Listening to port", PORT));
