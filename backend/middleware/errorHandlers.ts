import { Request, Response, NextFunction } from "express";

//Error handling
export const errorHandlerMiddleware = (
    error: Error,
    _req: Request,
    res: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _next: NextFunction
) => {
    console.error(error.message);

    if (error.name === "ValidationError") {
        return res.status(400).json({ error: error.message });
    }
    if (error.name === "SequelizeDatabaseError") {
        return res.status(500).json({ error: "Database error occurred" });
    } else {
        res.status(500).json({
            error: "Internal Server Error",
        });
    }
};

// Not Found
export const notFoundMiddleware = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const error = new Error(`Not Found - ${req.originalUrl}`);
    res.status(404);
    next(error);
};

//Logging the request
export const loggingMiddleware = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    console.log(
        `[${new Date().toISOString()}] Incoming request: ${req.method} ${
            req.url
        }`
    );
    next();
};
