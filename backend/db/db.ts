/* eslint-disable @typescript-eslint/no-explicit-any */
import pg from "pg";
import "dotenv/config";

const {
    PG_HOST,
    PG_PORT,
    PG_USERNAME,
    PG_PASSWORD,
    PG_DATABASE,
    DATABASE_URL,
} = process.env;

const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: String(PG_PASSWORD),
    database: PG_DATABASE,
    //NOTE(Suvi): You might not need this in your local.
    connectionString: DATABASE_URL,
});

export const executeQuery = async (
    query: string,
    parameters?: any[]
): Promise<pg.QueryResult<any> | undefined> => {
    const client = await pool.connect();
    try {
        const result: pg.QueryResult = await client.query(query, parameters);
        return result;
    } catch (error) {
        if (error instanceof Error) {
            console.error(error.stack);
            error.name = "dbError";
            throw error;
        }
    } finally {
        client.release();
    }
};

export const createUserTable = async (): Promise<void> => {
    const query = `
CREATE TABLE IF NOT EXISTS users (
    user_id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE
);`;
    await executeQuery(query);
    console.log("User table initialized");
};

export const createRecipesTable = async (): Promise<void> => {
    const query = `
CREATE TABLE IF NOT EXISTS recipes (
    recipe_id SERIAL PRIMARY KEY,
    recipe_title TEXT,
    recipe_link TEXT,
    recipe_notes TEXT[]
);`;
    await executeQuery(query);
    console.log("Recipes table initialized");
};

export const createFavoritesTable = async (): Promise<void> => {
    const query = `
CREATE TABLE IF NOT EXISTS favorites (
    favorite_id SERIAL PRIMARY KEY,
    user_id INT REFERENCES users(user_id) ON DELETE CASCADE,
    recipe_id INT REFERENCES recipes(recipe_id) ON DELETE CASCADE
);`;
    await executeQuery(query);
    console.log("Favorites table initialized");
};
