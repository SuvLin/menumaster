//USERS
export const insertUser = `
  INSERT INTO users (username) VALUES ($1) RETURNING *;
`;

export const findUser = `
SELECT * FROM users
WHERE user_id = $1`;

export const findAllUsers = `
SELECT * FROM users`;

export const updateUser = `
  UPDATE users
  SET username = $1
  WHERE user_id = $2
  RETURNING *;
`;

export const deleteUser = `
DELETE FROM users
WHERE user_id = $1
RETURNING *;`;

//RECIPES
export const findAllRecipes = `
  SELECT * FROM recipes
  ORDER BY recipe_title ASC;
`;

export const findARecipe = `
  SELECT * FROM recipes
  WHERE recipe_id = $1;
`;

export const insertRecipe = `
INSERT INTO recipes (recipe_title, recipe_link, recipe_notes)
VALUES ($1, $2, $3)
RETURNING *;`;

export const updateRecipe = `
UPDATE recipes
SET recipe_title = $2, recipe_link = $3, recipe_notes = $4
WHERE recipe_id = $1
RETURNING *;`;

export const deleteRecipe = `
DELETE FROM recipes
WHERE recipe_id = $1
RETURNING *;`;

export const searchRecipes = `
  SELECT * FROM recipes
  WHERE recipe_title ILIKE $1 OR $1 = ANY (recipe_notes);; 
`;

//FAVORITES

export const findAllUsersFavorites = `
SELECT recipes. *
FROM favorites
JOIN recipes ON favorites.recipe_id = recipes.recipe_id
WHERE favorites.user_id = $1;
`;

export const findFavoriteByUserIdAndRecipeId = `
SELECT * FROM favorites
WHERE user_id = $1 AND recipe_id = $2;`;

export const insertFavorite = `
INSERT INTO favorites (user_id, recipe_id)
VALUES ($1, $2)
RETURNING *`;

export const deleteFavorite = `
DELETE FROM favorites
WHERE user_id = $1 AND recipe_id = $2
RETURNING *;`;
