import { executeQuery } from "../db/db";
import * as queries from "../db/queries";

interface Favorite {
    favoriteId?: number;
    recipeId: number;
    userId: number;
}

const insertFavorite = async ({ userId, recipeId }: Favorite) => {
    console.log(`Adding to favorites....`);
    const values = [userId, recipeId];
    const result = await executeQuery(queries.insertFavorite, values);
    console.log(`Recipe added successfully to favorites!`);
    return result;
};

const findAllUsersFavorites = async (userId: number) => {
    console.log(`Fetching users favorites....`);
    const result = await executeQuery(queries.findAllUsersFavorites, [userId]);
    console.log(`Found ${result?.rows.length} favorites.`);
    return result;
};

const findFavoriteByUserIdAndRecipeId = async (
    userId: number,
    recipeId: number
) => {
    console.log(`Checking if favorite recipe...`);
    const values = [userId, recipeId];
    const result = await executeQuery(
        queries.findFavoriteByUserIdAndRecipeId,
        values
    );
    return result;
};

const deleteFavorite = async (userId: number, recipeId: number) => {
    console.log(`Removing recipe from favorites...`);
    const values = [userId, recipeId];
    const result = await executeQuery(queries.deleteFavorite, values);
    console.log(`Recipe removed successfully!`);
    return result;
};

export default {
    insertFavorite,
    findAllUsersFavorites,
    findFavoriteByUserIdAndRecipeId,
    deleteFavorite,
};
