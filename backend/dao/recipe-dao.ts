import { executeQuery } from "../db/db";
import * as queries from "../db/queries";

interface Recipe {
    recipe_id?: number;
    recipe_title: string;
    recipe_link?: string;
    recipe_notes?: string[];
}

const findOneRecipe = async (recipeId: number) => {
    console.log(`Getting a recipe with ${recipeId}....`);
    const result = await executeQuery(queries.findARecipe, [recipeId]);
    if (result) {
        console.log(`Recipe found!`);
        return result;
    } else {
        console.log("No recipe found or query failed");
        throw new Error("Recipe not found");
    }
};

const findAllRecipes = async () => {
    console.log(`Fetching recipes....`);
    const result = await executeQuery(queries.findAllRecipes);
    console.log(`Found ${result?.rows.length} recipes.`);
    return result;
};

const searchRecipes = async (keyword: string) => {
    console.log(`Searching...`);
    const searchKeyword = `%${keyword}%`;
    const result = await executeQuery(queries.searchRecipes, [searchKeyword]);
    console.log(`Search complete!`);
    return result;
};

const insertRecipe = async (recipe: Recipe) => {
    console.log(`Adding new recipe...`);
    // If recipe_notes is undefined or an empty array, default it to an empty array
    const notes = recipe.recipe_notes || [];
    const values = [recipe.recipe_title, recipe.recipe_link, notes];
    const result = await executeQuery(queries.insertRecipe, values);
    console.log(`New recipe added successfully!`);
    return result;
};

const updateRecipe = async (recipe: Recipe) => {
    console.log(`Updating recipe ${recipe.recipe_title}`);
    const values = [
        recipe.recipe_id,
        recipe.recipe_title,
        recipe.recipe_link,
        recipe.recipe_notes,
    ];
    const result = await executeQuery(queries.updateRecipe, values);
    return result;
};

const deleteRecipe = async (id: number) => {
    console.log(`Deleting recipe....`);
    const value = [id];
    const result = await executeQuery(queries.deleteRecipe, value);
    console.log(`Recipe deleted successfully!`);
    return result;
};

export default {
    findOneRecipe,
    findAllRecipes,
    searchRecipes,
    insertRecipe,
    updateRecipe,
    deleteRecipe,
};
