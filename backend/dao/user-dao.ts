import { executeQuery } from "../db/db";
import * as queries from "../db/queries";

interface User {
    user_id?: number;
    username: string;
}

const insertUser = async (user: User) => {
    console.log(`Adding new user...`);
    const values = [user];
    const result = await executeQuery(queries.insertUser, values);
    console.log(`New user ${user} has been added successfully!`);
    return result;
};

const findAllUsers = async () => {
    console.log("Requesting for all users...");
    const result = await executeQuery(queries.findAllUsers);
    return result;
};

const findUser = async (id: number) => {
    console.log(`Searching for user ${id}...`);
    const result = await executeQuery(queries.findUser, [id]);

    if (result) {
        console.log(`User found!`);
        return result;
    } else {
        console.log("No user found or query failed");
        throw new Error("User not found");
    }
};

const updateUser = async ({ username, user_id }: User) => {
    console.log(`Updating  user with id: ${user_id}...`);
    const values = [username, user_id];
    const result = await executeQuery(queries.updateUser, values);
    console.log(`User ${user_id} has been updated successfully!`);
    return result;
};

const deleteUser = async (id: number) => {
    console.log(`Deleting user with id: ${id}...`);
    const value = [id];
    const result = await executeQuery(queries.deleteUser, value);
    console.log(`User successfully deleted!`);
    return result;
};

export default { insertUser, findUser, findAllUsers, updateUser, deleteUser };
