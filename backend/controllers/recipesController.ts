import { Router, Request, Response, NextFunction } from "express";
import recipeDao from "../dao/recipe-dao";

const recipesController: Router = Router();

recipesController
    .route("/recipes")
    .post(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const recipe = req.body;
            const result = await recipeDao.insertRecipe(recipe);
            if (result && result.rows.length > 0) {
                const storedRecipe = result.rows[0];
                res.send(storedRecipe);
            } else {
                throw new Error("Recipe could not be created.");
            }
        } catch (error) {
            next(error);
        }
    })
    .get(async (_req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await recipeDao.findAllRecipes();
            res.send(result?.rows);
        } catch (error) {
            next(error);
        }
    });

recipesController.get(
    "/recipes/search",
    async (req: Request, res: Response, next: NextFunction) => {
        const keyword =
            typeof req.query.keyword === "string" ? req.query.keyword : "";
        try {
            const result = await recipeDao.searchRecipes(keyword);
            res.send(result?.rows);
        } catch (error) {
            console.log(`Search error: `, error);
            res.status(500).send("Error performing search");
        }
    }
);

recipesController
    .route("/recipes/:id")
    .get(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = parseInt(req.params.id);
            const result = await recipeDao.findOneRecipe(id);
            const recipe = result.rows[0];
            res.send(recipe);
        } catch (error) {
            next(error);
        }
    })
    .put(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const recipe_id = parseInt(req.params.id);
            const recipe = { recipe_id, ...req.body };
            const result = await recipeDao.updateRecipe(recipe);
            const updatedRecipe = result?.rows[0];
            res.send(updatedRecipe);
        } catch (error) {
            next(error);
        }
    })
    .delete(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = parseInt(req.params.id);
            const result = await recipeDao.deleteRecipe(id);
            res.send(result);
        } catch (error) {
            next(error);
        }
    });

export default recipesController;
