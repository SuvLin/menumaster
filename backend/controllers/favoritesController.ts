import { Router, Request, Response, NextFunction } from "express";
import favDao from "../dao/favorite-dao";

const favoritesController: Router = Router();

favoritesController
    .route("/users/:userid/favorites")
    .post(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userId = parseInt(req.params.userid);
            const { recipeId } = req.body;
            const result = await favDao.insertFavorite({ userId, recipeId });
            if (result && result.rows.length > 0) {
                const storedRecipe = result.rows[0];
                res.send(storedRecipe);
            } else {
                throw new Error("Recipe could not be added to favorites.");
            }
        } catch (error) {
            next(error);
        }
    })
    .get(async (req: Request, res: Response, next: NextFunction) => {
        const userId = parseInt(req.params.userid);
        try {
            const result = await favDao.findAllUsersFavorites(userId);
            res.send(result?.rows);
        } catch (error) {
            next(error);
        }
    });
favoritesController
    .route("/users/:userid/favorites/check/:recipeid")
    .get(async (req: Request, res: Response, next: NextFunction) => {
        const userId = parseInt(req.params.userid);
        const recipeId = parseInt(req.params.recipeid);
        try {
            const result = await favDao.findFavoriteByUserIdAndRecipeId(
                userId,
                recipeId
            );
            if (result && result.rows.length > 0) {
                res.send({ isFavorite: true });
            } else {
                res.send({ isFavorite: false });
            }
        } catch (error) {
            next(error);
        }
    })
    .delete(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userId = parseInt(req.params.userid);
            const recipeId = parseInt(req.params.recipeid);
            const result = await favDao.deleteFavorite(userId, recipeId);
            res.send(result?.rows[0]);
        } catch (error) {
            next(error);
        }
    });

export default favoritesController;
