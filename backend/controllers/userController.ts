import { Router, Request, Response, NextFunction } from "express";
import userDao from "../dao/user-dao";

const userController: Router = Router();

userController
    .route("/users")
    .get(async (_req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await userDao.findAllUsers();
            res.send(result?.rows);
        } catch (error) {
            next(error);
        }
    })
    .post(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const user = req.body;
            if (!user.username) {
                throw new Error("Username required!");
            }
            const result = await userDao.insertUser(user.username);
            if (result && result.rows.length > 0) {
                const storedUser = result.rows[0];
                res.send(storedUser);
            } else {
                throw new Error("User could not be created.");
            }
        } catch (error) {
            next(error);
        }
    });

userController
    .route("/users/:id")
    .get(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = parseInt(req.params.id);
            const result = await userDao.findUser(id);
            const user = result.rows[0];
            res.send(user);
        } catch (error) {
            next(error);
        }
    })
    .put(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = parseInt(req.params.id);
            const { username } = req.body;
            const user = { user_id: id, username };
            await userDao.updateUser(user);
            res.send(user);
        } catch (error) {
            next(error);
        }
    })
    .delete(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id = parseInt(req.params.id);
            const result = await userDao.deleteUser(id);
            res.send(result);
        } catch (error) {
            next(error);
        }
    });

export default userController;
